# SPDX-FileCopyrightText: 2020 Carl Schwan <carl@carlshwan.eu>
# SPDX-FileCopyrightText: 2021-2022 Nguyen Hung Phu <phu.nguyen@kdemail.net>
# SPDX-License-Identifier: LGPL-2.0-or-later

from __future__ import annotations

import configparser
import datetime
import importlib.resources as pkg_resources
import json
import logging
import os
import requests
import subprocess

import polib
from markdown_it import MarkdownIt
from yaml import safe_load, dump

from hugoi18n import command_line, generation


def fetch_flathub() -> dict:
    def condition_id(id_str: str) -> bool:
        return id_str.startswith('org.kde.') or id_str.startswith('im.kaidan.')

    all_apps = requests.get("https://flathub.org/api/v1/apps").json()
    kde_apps = {app['flatpakAppId']: {
        'downloads': 0,
        'first_date': '',
        'last_date': '',
        'downloads_per_day': 0,
        'popularity': 0
    } for app in all_apps if condition_id(app['flatpakAppId'])}

    today = datetime.date.today()
    year, month = today.year, today.month-1
    if month == 0:
        month = 12
        year -= 1

    stats_path = f'stats/{year}/{month:02}'
    if not os.path.isdir(stats_path):
        if subprocess.call(["wget", "--recursive", "--no-host-directories", "--no-parent", "--reject-regex", "backup",
                            "--accept", "*.json", f'https://flathub.org/{stats_path}/']) != 0:
            return kde_apps

    files = []
    for (dirpath, _, filenames) in os.walk(stats_path):
        files.extend(os.path.join(dirpath, filename) for filename in filenames)
    files.sort()

    for f in files:
        with open(f) as f_json:
            data = json.load(f_json)
            for app_id in data['refs']:
                if app_id in kde_apps:
                    if not kde_apps[app_id]['first_date']:
                        kde_apps[app_id]['first_date'] = data['date']
                    else:
                        kde_apps[app_id]['last_date'] = data['date']
                    arch_sum = sum([value[0] for value in data['refs'][app_id].values()])
                    kde_apps[app_id]['downloads'] += arch_sum
    # shutil.rmtree('stats')

    for app_id in kde_apps:
        if kde_apps[app_id]['downloads'] == 0:
            kde_apps[app_id]['downloads_per_day'] = 0
        else:
            first_date = datetime.datetime.strptime(kde_apps[app_id]['first_date'], '%Y/%m/%d')
            last_date = datetime.datetime.strptime(kde_apps[app_id]['last_date'], '%Y/%m/%d')
            days = (last_date - first_date).days + 1
            kde_apps[app_id]['downloads_per_day'] = kde_apps[app_id]['downloads'] / days
    download_sum_per_day = sum([kde_apps[app_id]['downloads_per_day'] for app_id in kde_apps])
    for app_id in kde_apps:
        kde_apps[app_id]['popularity'] = kde_apps[app_id]['downloads_per_day'] / download_sum_per_day * 100
    return kde_apps


def fetch_archlinux() -> dict:
    with open('app-id-matches.yaml') as f:
        kde_apps = safe_load(f)
        for app_id in kde_apps:
            app_name = kde_apps[app_id]['archlinux']['name']
            if app_name:
                url = f'https://pkgstats.archlinux.de/api/packages/{app_name}'
                res = requests.get(url)
                popularity = res.json()['popularity']
                # print(f'{app_name}: {popularity}')
                kde_apps[app_id]['archlinux']['popularity'] = popularity
        return kde_apps


def get_snap_url(common_id):
    req_url = f'http://api.snapcraft.io/v2/snaps/find?fields=store-url&common-id={common_id}'
    headers = {'Snap-Device-Series': '16'}
    try:
        req = requests.get(req_url, headers=headers)
        url = req.json()['results'][0]['snap']['store-url']
    except (requests.exceptions.ConnectionError, ValueError) as err:
        logging.exception(f'{common_id}: {err}')
        url = ''
    except IndexError:
        url = ''
    return url


def prepare_app_data():
    app_data = []
    app_counts = {}

    plasma_addons = ['org.kde.plasma.katesessions', 'org.kde.plasma.printmanager', 'org.kde.plasma.wacomtablet',
                     'org.kde.plasma.worldclock']
    non_apps = ['org.kde.atcore', 'org.kde.kdialog', 'org.kde.khelpcenter', 'org.kde.sieveeditor']
    to_ignore = ['.gitignore'] + plasma_addons + non_apps
    for file_name in os.listdir('static/appdata'):
        if os.path.splitext(file_name)[0] in to_ignore:
            continue

        with open(f'static/appdata/{file_name}', 'r') as f:
            data = safe_load(f)

        # check that data['ID'] always starts with file_name excluding extension
        # print(f"{file_name} - {data['ID']}")

        data['snap_url'] = get_snap_url(data['ID'])
        app_data.append(data)

        for lang_code in data['Summary']:
            hugo_lang_code = 'en' if lang_code == 'C' else generation.convert_lang_code(lang_code)
            if hugo_lang_code in app_counts:
                app_counts[hugo_lang_code] += 1
            else:
                app_counts[hugo_lang_code] = 1

    # only process languages with more than 50 translated apps
    working_lang_codes = dict(filter(lambda elem: elem[1] > 50, app_counts.items())).keys()
    return app_data, working_lang_codes


def write_file_frontmatter(path: str, fm: dict):
    with open(path, 'w') as f:
        f.write('---\n')
        f.write(dump(fm, allow_unicode=True))
        f.write('---\n')


def gen_term_file(taxo, term, hugo_lang_code, tr=None):
    file_path = f'content/{taxo}/{term}/_index.{hugo_lang_code}.md'
    lang_prefix = '' if hugo_lang_code == 'en' else f'/{hugo_lang_code}'
    content = {
        'title': tr(term.capitalize()) if tr else term.capitalize(),
        'aliases': [f'{lang_prefix}/{term}']
    }
    if term in cats_with_subcats:
        content['layout'] = f'{term}_cat'
    write_file_frontmatter(file_path, content)


def gen_subcat_file(cat, subcat, hugo_lang_code, lang_subcat_name):
    file_path = f'content/categories/{cat}/{subcat}.{hugo_lang_code}.md'
    lang_prefix = '' if hugo_lang_code == 'en' else f'/{hugo_lang_code}'
    content = {
        'title': lang_subcat_name,
        'aliases': [f'{lang_prefix}/{cat}/{subcat}'],
        'layout': f'{cat}_subcat'
    }
    write_file_frontmatter(file_path, content)


def gen_categories(working_lang_codes):
    # subcategory icon names have '-'
    categories = [os.path.splitext(x)[0] for x in os.listdir('static/app-icons/categories') if '-' not in x]
    categories.remove('settingsmenu')

    # translate category names
    # we can also use desktop files in plasma-workspace/share/desktop-directories
    # but PO messages are grouped by languages, much more convenient to work with
    # than strings in those desktop files
    po_dir = 'pos_cat'
    os.environ['PACKAGE'] = 'plasma-workspace'
    os.environ['FILENAME'] = 'plasma-workspace._desktop_'
    command_line.fetch_langs(working_lang_codes, True, po_dir)

    for cat in categories:
        os.makedirs(f'content/categories/{cat}', exist_ok=True)

    for hugo_lang_code in working_lang_codes:
        strings = {}
        lang_code = generation.revert_lang_code(hugo_lang_code)
        po_path = f'{po_dir}/{lang_code}.po'
        if os.path.isfile(po_path):
            po_file = polib.pofile(po_path)

            def translate(msgid: str) -> str:
                return po_file.find(msgid).msgstr
        else:
            def translate(msgid: str) -> str:
                return msgid

        for cat in categories:
            gen_term_file('categories', cat, hugo_lang_code, translate)
            strings[cat] = {'other': translate(cat.capitalize())}
        for cat in cats_gen_subcat_files:
            for subcat_code, subcat_name in cats_subcats[cat].items():
                lang_subcat_name = translate(subcat_name)
                gen_subcat_file(cat, subcat_code, hugo_lang_code, lang_subcat_name)
                strings[f'{cat}-{subcat_code}'] = {'other': lang_subcat_name}
        for cat in cats_not_gen_subcat_files:
            for subcat_code, subcat_name in cats_subcats[cat].items():
                strings[f'{cat}-{subcat_code}'] = {'other': translate(subcat_name)}

        i18n_path = f'i18n/{hugo_lang_code}.yaml'
        with open(i18n_path, 'a') as f_i18n:
            f_i18n.write(dump(strings, default_flow_style=False, allow_unicode=True))
    # shutil.rmtree(po_dir)


def get_content(data, lang_code: str) -> str:
    return data.get(lang_code, data['C'])


def gen_app(data, shortname: str, lang_code: str, hugo_lang_code: str):
    content = dict()
    content['name'] = get_content(data['Name'], lang_code)
    content['title'] = get_content(data['Name'], lang_code)
    content['summary'] = get_content(data['Summary'], lang_code)
    content['description'] = get_content(data['Summary'], lang_code)
    if 'Description' in data:
        content['appDescription'] = get_content(data['Description'], lang_code)

    if 'X-KDE-GenericName' in data and 'C' in data['X-KDE-GenericName']:
        content['GenericName'] = get_content(data['X-KDE-GenericName'], lang_code)
    else:
        content['GenericName'] = get_content(data['Summary'], lang_code)

    if 'Url' in data:
        urls = data['Url']
        content['homepage'] = urls.get('homepage', '')
        if 'games.kde.org' in content['homepage']:
            content['homepage'] = ''
        content['bugtracker'] = urls.get('bugtracker', '')
        content['help'] = urls.get('help', '')
        content['donation'] = urls.get('donation', '')

    content['ProjectLicense'] = data.get('ProjectLicense', '')
    content['KDEProject'] = data.get('X-KDE-Project', '')
    content['repository'] = data.get('X-KDE-Repository', '')
    content['ProjectGroup'] = data.get('ProjectGroup', 'KDE')

    content['appType'] = data['Type']

    # these are unmaintained but their names or categories do not indicate so
    # https://invent.kde.org/websites/kde-org-applications-extractor/-/blob/48148db5de8f3578e00d57926b4d9d5b8aa4eb47/unmaintained.json
    unmaintained_exceptions = ['calligragemini', 'karbon', 'kst']
    if content['KDEProject'].startswith('unmaintained') or shortname in unmaintained_exceptions:
        content['Categories'] = ['unmaintained']
    elif 'Categories' in data and len(data['Categories']) > 0:
        content['Categories'] = data['Categories']
        content['Categories'].sort()
    elif content['appType'] == 'addon':
        content['Categories'] = ['addons'] if not shortname.startswith('okular-') else ['office']
    elif shortname == 'kitinerary-extractor':
        content['Categories'] = ['office', 'utilities']
    else:
        content['Categories'] = ['unmaintained']

    content['MainCategory'] = content['Categories'][0]
    if len(content['Categories']) > 1 and '/' in content['repository']:
        repo_category = content['repository'].split('/')[0]
        if repo_category in content['Categories']:
            content['MainCategory'] = repo_category
        elif repo_category == 'pim' and 'office' in content['Categories']:
            content['MainCategory'] = 'office'
    # now 'Categories' is a list of lower-case English names of categories, which can be considered category 'codes';
    #     localized category names are in files in 'content/categories' directory;
    #     'MainCategory' is the first element in 'Categories', i.e. the first category 'code'.
    content['subcategories'] = data['subcategories']

    content['id'] = data['X-KDE-ID']
    if content['id'] == 'org.kde.kdeconnect.kcm':
        content['appType'] = 'desktop-application'
    content['flathub'] = content['id'] in flathub_apps
    content['snap_url'] = data['snap_url']

    for job in windows_nightly_data:
        if data['Name']['C'] in job['name'] and 'Release' in job['name']:
            content['windowsRelease'] = job
            continue
        if data['Name']['C'] in job['name'] and 'Nightly' in job['name']:
            content['windowsNightly'] = job
            continue

    platforms = set()
    if 'Custom' in data:
        customs = data['Custom']
        content['forum'] = customs.get('KDE::forum', '')
        content['irc'] = customs.get('KDE::irc', '')
        content['mailinglist'] = customs.get('KDE::mailinglist', '')
        if content['mailinglist'].endswith('@kde.org'):
            content['mailinglist'] = content['mailinglist'].replace('@kde.org', '')
        content['matrixChannel'] = customs.get('KDE::matrix', '')
        content['develMailingList'] = customs.get('KDE::mailinglist-devel', '')
        if content['develMailingList'].endswith('@kde.org'):
            content['develMailingList'] = content['develMailingList'].replace('@kde.org', '')
        if 'KDE::windows_store' in customs:
            content['windowsStoreLink'] = customs['KDE::windows_store']
            platforms.add("windows")
        if 'KDE::f_droid' in customs:
            content['fDroidLink'] = customs['KDE::f_droid']
            platforms.add("android")
        if 'KDE::google_play' in customs:
            content['googlePlayLink'] = customs['KDE::google_play']
            platforms.add("android")
    content['platforms'] = list(platforms)

    flathub_popularity = flathub_apps[content['id']]['popularity'] if content['id'] in flathub_apps else 0
    archlinux_popularity = archlinux_apps[content['id']]['archlinux'].get('popularity', 0)\
        if content['id'] in archlinux_apps else 0
    flathub_weight = 0.5
    content['popularity'] = flathub_popularity * flathub_weight + archlinux_popularity * (1 - flathub_weight)

    content['screenshots'] = list()
    if 'Screenshots' in data:
        for screenshot in data['Screenshots']:
            screenshot_obj = dict()
            if 'caption' in screenshot:
                screenshot_obj['caption'] = get_content(screenshot['caption'], lang_code)
            screenshot_obj['url'] = screenshot['source-image']['url']
            # TODO generate in case of missing
            # screenshot_obj['width'] = screenshot['source-image']['width']
            # screenshot_obj['height'] = screenshot['source-image']['height']
            content['screenshots'].append(screenshot_obj)
        content['images'] = [screenshot['source-image']['url'] for screenshot in data['Screenshots']]

    content['releases'] = list()
    if 'Releases' in data:
        latest_stable_release = None
        latest_stable_release_timestamp = -1
        latest_development_release = None
        latest_development_release_timestamp = -1
        for release in data['Releases']:
            release_obj = dict()
            release_obj['version'] = release['version']
            if 'unix-timestamp' in release:
                release_obj['date'] = datetime.date.fromtimestamp(int(release['unix-timestamp'])).isoformat()
            else:
                continue

            if 'url' in release:
                release_obj['url'] = release['url']['details']

            if 'description' in release:
                release_obj['description'] = get_content(release['description'], lang_code)
            if 'artifacts' in release:
                release_obj['artifacts'] = release['artifacts']

            if release['type'] == "stable" and release["unix-timestamp"] > latest_stable_release_timestamp:
                latest_stable_release_timestamp = release["unix-timestamp"]
                latest_stable_release = release_obj

            if release['type'] == "development" and release["unix-timestamp"] > latest_development_release_timestamp:
                latest_development_release_timestamp = release["unix-timestamp"]
                latest_development_release = release_obj

            content['releases'].append(release_obj)

        if latest_development_release:
            content['latestDevelopmentRelease'] = latest_development_release
        if latest_stable_release:
            content['latestStableRelease'] = latest_stable_release

    content['aliases'] = [f"/{content['id']}",
                          f"/{content['MainCategory']}/{content['id']}",
                          f"/{content['MainCategory']}/{shortname}"] if hugo_lang_code == "en" \
        else [f"/{hugo_lang_code}/{content['id']}",
              f"/{hugo_lang_code}/{content['MainCategory']}/{content['id']}",
              f"/{hugo_lang_code}/{content['MainCategory']}/{shortname}"]

    if 'Extends' in data:
        content['extends'] = data['Extends']
    if 'Icon' in data:
        content['icon'] = data['Icon']['local'][0]['name']
    content['desktopId'] = data['ID']

    return content


def get_subcat_names(cat: str) -> dict[str, str]:
    if cat == 'education':
        cat = 'edu'
    subcat_desktop_files = [x for x in os.listdir(desktopDirPath) if x.startswith(f'kf5-{cat}-')]
    subcat_pairs = {}
    for subcat_desktop_file in subcat_desktop_files:
        subcat_code = subcat_desktop_file.split('.')[0].split('-')[2]
        with open(f'{desktopDirPath}/{subcat_desktop_file}') as f_desktop:
            cp = configparser.ConfigParser()
            cp.read_file(f_desktop)
            section = cp['Desktop Entry']
            subcat_pairs[subcat_code] = section['Name']
    return subcat_pairs


def gen_index_file(section, hugo_lang_code, tr=None):
    if section == 'home':
        file_path = f'content/_index.{hugo_lang_code}.md'
        lang_prefix = '' if hugo_lang_code == 'en' else f'/{hugo_lang_code}'
        title = 'KDE Applications'
        content = {
            'aliases': [f'{lang_prefix}/applications'],
            'title': tr(title) if tr else title
        }
    else:
        file_path = f'content/{section}/_index.{hugo_lang_code}.md'
        title = 'Other platforms' if section == 'platforms' else section.capitalize()
        content = {
            'menu': {'main': {'weight': 2 if section == 'categories' else 3}},
            'title': tr(title) if tr else title
        }
    write_file_frontmatter(file_path, content)


def gen_platforms(hugo_lang_code, tr=None):
    gen_index_file('platforms', hugo_lang_code, tr)

    for platform in other_platforms:
        gen_term_file('platforms', platform, hugo_lang_code, tr)


def gen_others(working_lang_codes):
    en_strings, configs, original_configs, src_data = generation.read_sources()

    lang_config = list(configs['languages'].keys())
    with pkg_resources.open_text('hugoi18n.resources', 'languages.yaml') as f_langs:
        lang_names = safe_load(f_langs)
    for config_lang_code in lang_config:
        if config_lang_code not in working_lang_codes:
            del configs['languages'][config_lang_code]

    for platform in other_platforms:
        os.makedirs(f'content/platforms/{platform}', exist_ok=True)
    os.makedirs(f'content/categories/unmaintained', exist_ok=True)
    mdi = MarkdownIt(renderer_cls=generation.LocalizedMdRenderer).enable('table')
    for hugo_lang_code in working_lang_codes:
        if hugo_lang_code in ['en']:
            gen_platforms(hugo_lang_code)
            gen_index_file('categories', hugo_lang_code)
            gen_index_file('home', hugo_lang_code)
            gen_term_file('categories', 'unmaintained', hugo_lang_code)
        else:
            lang_code = generation.revert_lang_code(hugo_lang_code)
            os.environ["LANGUAGE"] = lang_code
            tr = generation.gettext_func(os.environ['FILENAME'])

            generation.generate_strings(en_strings, hugo_lang_code, tr)
            generation.generate_languages(configs, lang_names, lang_code, hugo_lang_code)
            generation.generate_menu(configs, hugo_lang_code, tr)
            generation.generate_description(configs, hugo_lang_code, tr)
            generation.generate_title(configs, hugo_lang_code, tr)
            generation.generate_data_files(configs['i18n']['excludedDataKeys'].split(),
                                           src_data, hugo_lang_code, tr, mdi)

            gen_platforms(hugo_lang_code, tr)
            gen_index_file('categories', hugo_lang_code, tr)
            gen_index_file('home', hugo_lang_code, tr)
            gen_term_file('categories', 'unmaintained', hugo_lang_code, tr)

    generation.write_target(configs, original_configs)


if __name__ == "__main__":
    level = logging.INFO
    logging.basicConfig(format='%(levelname)s: %(message)s', level=level)

    subprocess.run(['python3', 'main.py'], cwd='appdata-extractor/')

    flathub_apps = fetch_flathub()
    archlinux_apps = fetch_archlinux()
    windows_nightly_data = requests.get("https://binary-factory.kde.org/view/Windows%2064-bit/api/json").json()["jobs"]

    all_app_data, working_langs = prepare_app_data()

    os.makedirs('content/applications', exist_ok=True)
    for one_app in all_app_data:
        parts = one_app['ID'].split('.')[2:-1] if one_app['ID'].endswith('.desktop') else one_app['ID'].split('.')[2:]
        app_shortname = '.'.join(parts)
        # index.html
        if app_shortname == 'index':
            app_shortname = 'index-fm'

        for lang in one_app['Summary']:
            hugo_lang = 'en' if lang == 'C' else generation.convert_lang_code(lang)
            if hugo_lang in working_langs:
                app_content = gen_app(one_app, app_shortname, lang, hugo_lang)
                output_file = f'content/applications/{app_shortname}.{hugo_lang}.md'
                write_file_frontmatter(output_file, app_content)

    logging.info('Applications done')

    other_platforms = ['android', 'windows']
    # create directory before we can generate index files in 'gen_others'
    os.makedirs('content/categories', exist_ok=True)
    # we need to have cats_with_subcats before it can be used in gen_term_file inside gen_others
    plasmaworkspaceDirPath = 'appdata-extractor/plasma-workspace'
    # plasmaworkspaceDirPath = '/usr'
    desktopDirPath = f'{plasmaworkspaceDirPath}/share/desktop-directories'
    # key: category code
    # value: dict of (subcategory code, subcategory name)
    cats_subcats = {}
    # categories with subcategories that we will process
    cats_with_subcats = ['games']
    # categories not to generate subcategory files for
    cats_not_gen_subcat_files = ['games']
    # categories to generate subcategory files for
    cats_gen_subcat_files = (x for x in cats_with_subcats if x not in cats_not_gen_subcat_files)

    for category in cats_with_subcats:
        subcat_names = get_subcat_names(category)
        cats_subcats[category] = subcat_names
        # gen subcat list data
        with open(f'data/{category}_subcats.yaml', 'w') as f_subcats:
            f_subcats.write(dump(list(subcat_names.keys())))

    # translate strings, title, description, and menu
    app_po_dir = 'pos_app'
    os.environ['PACKAGE'] = 'websites-apps-kde-org'
    os.environ['FILENAME'] = 'kde-org-applications'
    command_line.fetch_langs(working_langs, True, app_po_dir)
    command_line.compile_po_in(app_po_dir)
    gen_others(working_langs)
    # shutil.rmtree('locale')
    # shutil.rmtree(po_dir)
    logging.info('L10n done')

    # categories and subcategories generation: this needs to be after i18n files have been generated
    gen_categories(working_langs)
    logging.info('Categories done')
